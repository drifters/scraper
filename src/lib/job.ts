import * as queue from 'async/queue';
import {EventEmitter} from 'events';
import {SPIDER_JOB_STATUS} from './state';
import {ISpiderTask, ISpiderTaskType} from '..';
import {Spider} from './spider';

export class ScraperJob extends EventEmitter {

    private tasks: ISpiderTask[] = [];
    private failedJobs: ISpiderTask[] = [];
    private successfulJobs: ISpiderTask[] = [];
    private readonly options: { scraperName: any, enableProxy: boolean };
    private q: any = null;
    private queueSize: number = 1;
    private time: any = {};

    /**
     * scraper job
     * @param scraperName
     * @param enableProxy
     */
    constructor({
                    scraperName,
                    enableProxy = false
                }: {
        scraperName: String,
        enableProxy: boolean
    }) {
        super();
        this.options = {scraperName, enableProxy};
    }

    /**
     * used to initialize job queue
     */
    private initialize = () => {
        const { scraperName, enableProxy } = this.options;
        this.time.start = new Date();
        this.q = queue(async (task: ISpiderTask, currentTaskCallback: (err: any, success?: any) => {}) => {
            this.emit(SPIDER_JOB_STATUS.TASK_STARTED, {
                scraperName,
                task
            });
            try {
                const { type = ISpiderTaskType.SCRAPE_URI, resourcePath } = task;

                const spider = new Spider({
                    ...task,
                    enableProxy: task.enableProxy || enableProxy || false
                });
                const options = (task.type === ISpiderTaskType.DOWNLOAD) ? <any>{ resourcePath } : {}
                const data = await spider[type](options)
                currentTaskCallback(null, data);
            } catch (err) {
                try { // handle any client implementation error if any
                    currentTaskCallback(err);
                }catch (e) {
                    console.log("Issue with client implementation", err);
                }
            }
        }, this.queueSize);

        this.q.drain(() => {
            this.time.end = new Date();
            this.emit(SPIDER_JOB_STATUS.COMPLETED, {
                scraperName
            });
        });

        this.q.error((data: any, task: any) => {
            this.failedJobs.push(task);
            this.emit(SPIDER_JOB_STATUS.TASK_ERR, {
                scraperName,
                task,
                ...data
            });
        });

        this.emit(SPIDER_JOB_STATUS.INITIALIZED, {
            scraperName
        });
    }


    /**
     * add to queue
     * @param task
     */
    private addTaskToQueue = (task: ISpiderTask) => {
        const {scraperName} = this.options;
        if (this.q === null) { //initialize queue if not initialized
            this.initialize();
        }

        // update task id
        task.id = this.tasks.length;
        this.tasks.push(task);

        this.q.push(task, (err: any, data: any) => {
            if (!err) {
                this.successfulJobs.push(task);
                this.emit(SPIDER_JOB_STATUS.TASK_FINISHED, {
                    scraperName,
                    task,
                    ...data
                });
            }
        });
    };

    /**
     * set the queue size
     * @param size
     */
    setQueueSize = (size: number) => {
        this.queueSize = size;
    }

    /**
     * add to tasks
     * @param tasks
     */
    scrape = (tasks = this.tasks) => {
        if (tasks && tasks.length > 0) {
            //this.tasks = this.tasks.concat(tasks);
            tasks.forEach((task) => this.addTaskToQueue(task))
        }
    };

    getFailedTasks = () => {
        return this.failedJobs;
    }
    /**
     * get the current status
     */
    getCurrentStatus = () => ({
        total: this.tasks.length,
        successfulJobs: this.successfulJobs.length,
        failureJobs: this.failedJobs.length,
        successRatio: (this.successfulJobs.length / this.tasks.length) * 100,
        failureRatio: (this.failedJobs.length / this.tasks.length) * 100,
        startTime: this.time.start,
        endTime: this.time.end,
        elapsedTime: `${(this.time.end - this.time.start) / 1000} sec.`,
        elapsedTimePerPage: `${((this.time.end - this.time.start) / 1000) / this.tasks.length} sec.`
    })
}
