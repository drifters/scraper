function noOp() {};

exports.Cleanup = function Cleanup(callback: any) {

    // attach user callback to the process event emitter
    // if no callback, it will still exit gracefully on Ctrl-C
    callback = callback || noOp;
    // @ts-ignore
    process.on('cleanup',callback);

    // do app specific cleaning before exiting
    process.on('exit', function (e) {
        console.log('sdfs',e);
        // @ts-ignore
        process.emit('cleanup');
    });

    // catch ctrl+c event and exit normally
    process.on('SIGINT', function (e) {
        console.log('Ctrl-C...',e);
        process.exit(2);
    });

    //catch uncaught exceptions, trace, then exit normally
    process.on('uncaughtException', function(e) {
        console.log(e);
        console.log('Uncaught Exception...');
       // console.log(e.stack);
        process.exit(99);
    });
};

// Prevents the program from closing instantly
process.stdin.resume();