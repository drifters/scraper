
export enum SPIDER_STATUS {
    HEAD_ERR = "HEAD_ERR",
    SCRAPE_SUCCESS = "SCRAPE_SUCCESS",
    SCRAPE_ERR = "SCRAPE_ERR",
    DOWNLOAD_SUCCESS = "DOWNLOAD_SUCCESS",
    DOWNLOAD_ERR = "DOWNLOAD_ERR",
    COOKIES_CAPTURED = "COOKIES_CAPTURED"
}

export const SPIDER_JOB_STATUS = Object.freeze({
    INITIALIZED: String("INITIALIZED"),
    COMPLETED: String("COMPLETED"),
    TASK_STARTED: String("TASK_STARTED"),
    TASK_ERR: String("TASK_ERR"),
    TASK_FINISHED: String("TASK_FINISHED"),
    // INVALID_TASK: String("INVALID_TASK"),
});
