/// <reference types="../../types/ambient" />
import {default as axios_} from 'axios'
import { InitializeInterceptors } from './interceptor';



export const InitializeRequest = ({ enableProxy = false }: any) => {
    let axiosInstance = null
    if (enableProxy) {
        const SocksProxyAgent = require('socks-proxy-agent');
        const proxyHost = '127.0.0.1';
        const proxyPort = '9050';
        const proxyOptions = `socks5://${proxyHost}:${proxyPort}`;
        let httpsAgent = new SocksProxyAgent(proxyOptions)
        let httpAgent = httpsAgent
        axiosInstance = axios_.create({httpsAgent,httpAgent, withCredentials: true});
    } else {
        axiosInstance = axios_.create({ withCredentials: true});
    }

    InitializeInterceptors(axiosInstance);
    return axiosInstance;
};
