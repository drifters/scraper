import * as cheerio from 'cheerio';
import {EventEmitter} from 'events';
import * as fs from 'fs';
import {dirname, resolve as pathResolve} from 'path';
import * as url from "url";
import {v4 as uuidv4} from 'uuid';
import {InitializeRequest} from './request';
import {SPIDER_STATUS} from './state';

const UserAgent = require('user-agents');
const cliProgress = require('cli-progress');
const mimeData = require('../db/mime');
const puppeteer = require('puppeteer');
const crypto = require('crypto');
const WebSocket = require('ws');
const qs = require('querystring')

export class Spider extends EventEmitter {
    private uri: any;
    private enableProxy: boolean;
    private readonly scheme: string;
    private readonly request: any;
    private puppetBrower: any;
    private page: any;
    private readonly task: any = [];
    private mime: string = "";
    private readonly userAgent: string;
    private requestOptions: any = {};

    /**
     * Instance to scrape a given uri and either download or enable parsing over it
     * @param id
     * @param name
     * @param uri Uri to download
     * @param enableProxy if proxy needs be used for making request
     * @param scheme
     */
    constructor({
        id = uuidv4(),
        name,
        uri,
        enableProxy = false,
        scheme = 'http',
        userAgent
    }: {
        id?: string | number,
        name?: string
        uri: string,
        enableProxy?: boolean
        scheme?: string
        userAgent?: string
    }) {
        super();
        this.task = {id, name, uri};
        this.uri = uri;
        this.userAgent = userAgent || (new UserAgent()).toString();
        this.enableProxy = enableProxy;
        this.scheme = url.parse(uri).protocol || scheme;
        this.request = InitializeRequest({ scheme, enableProxy });
    }

    //emit(a:any,b:any){};
    //on(a:any,b:any){}


    /**
     * check if can read and write file to targetPath
     * @param targetPath
     */
    private canReadAndWrite = async (targetPath: fs.PathLike): Promise<boolean> => {
        return new Promise((resolve, reject) => {
            fs.stat(targetPath, (err) => {
                if (err) {
                    reject(err);
                    return;
                }
                fs.access(targetPath, (fs.constants || fs).W_OK | (fs.constants || fs).R_OK, (err) => {
                    if (err) {
                        const dir = dirname(targetPath.toString());
                        fs.access(dir, (fs.constants || fs).W_OK | (fs.constants || fs).R_OK, (err) => {
                            if (err) {
                                reject(err);
                                return;
                            }
                            resolve(false);
                        });
                    }
                    resolve(true);
                })
            });
        });
    };

    /**
     * create path if not exits
     * @param resourcePath
     */
    private createDirIfNotExits = async ({ resourcePath }: { resourcePath: string }) => {
        return new Promise(async (resolve, reject) => {
            try {
                await this.canReadAndWrite(pathResolve(resourcePath));
                resolve(true);
            }catch (e) {
                fs.mkdir(resourcePath, { recursive: true }, (err) => {
                    if (err) {
                        return reject(err)
                    }
                    resolve(true);
                });
            }
        });
    }

    /**
     * parsing json
     * @param data
     * @param data
     */
    private tryJSONParse = (data: string) => {
        try {
            return JSON.parse(data);
        } catch (e) {
            return data
        }
    };

    /**
     * encode uri to string
     * @param uri
     */
    private encodeUriToString = (uri: string) => Buffer.from(uri).toString('base64');


    private wait = async (ms: any ) => {
        return new Promise(resolve => setTimeout(() => resolve(), ms));
    }

    private isCaptchaSite = ($ : any) => {
        return [($('#cf-wrapper').length > 0)]
            .some((condition) => condition)
    }

    private registerCDPListener = async () => {
        return new Promise(async (resolve, reject) => {
            if(this.page._client) {
                const socket = this.page._client;
                socket._connection.on('Target.targetDestroyed', async (params: any) => {
                    process.exit(0);
                });
                socket._connection.on('Target.targetInfoChanged', async (params: any) => {
                    const { targetInfo: { title = "" }} = params;
                    if (title.includes('__cf_chl_captcha_tk__') && title.includes(this.uri)) {
                        console.log({params});
                        const { cookies } = await socket.send('Network.getAllCookies');
                        console.log(JSON.stringify(cookies, null, 4));
                        this.emit(SPIDER_STATUS.COOKIES_CAPTURED, { task: this.task, cookies });
                        resolve({ cookies, uri: title });
                    }else{
                        console.log(params)
                    }
                })
                console.log('Listening to browser socket events');
                await socket._connection.send('Target.setDiscoverTargets',  {
                    discover: true
                });
            } else {
                const ws =  new WebSocket(this.puppetBrower.wsEndpoint(), {perMessageDeflate: true});

                console.log(ws);
                await new Promise(resolve => ws.once('open', resolve));
                console.log('connected!');

                ws.on('message', (msg: any) => {
                    const data = JSON.parse(msg);
                    const methods: any = {
                        "Target.targetDestroyed": () => {
                            process.exit(1);
                        },
                        "Target.targetInfoChanged": () => {
                            // console.log(JSON.stringify(data, null, 4));
                            const {params: {targetInfo: { title = "" }}} = data;
                            if (title.includes('__cf_chl_captcha_tk__') && title.includes(this.uri)) {
                                console.log({msg});
                                ws.send('Network.getAllCookies')
                            }
                        }
                    }
                    if (data.method) {
                        methods[data.method] && methods[data.method]() || console.log({method: data.method});
                    } else {
                        console.log({data})
                    }
                });
            }
        })
    }

    /**
     * get the fileExtension for a
     * @param contentType
     */
    private setDefaultFileExtension = (contentType: string) => {
        const type = contentType.split(';')[0]
        this.mime = mimeData[type];
    }

    /**
     * open window when captcha occurs upon resolving capture cookies and retry
     * @param uri
     */
    executeHtml = async () => {
        return new Promise(async (resolve, reject) => {
            try {
                const uri = this.uri;
                const userAgent = this.userAgent;
                if(!this.puppetBrower) {
                    this.puppetBrower = await puppeteer.launch({
                        args: ['--no-sandbox', '--proxy-server=socks5://localhost:9050', '--remote-debugging-port=0'],
                        headless: false
                    });
                }
                if(!this.page) {
                    this.page = await this.puppetBrower.newPage();
                    this.page.setUserAgent(userAgent)
                }
                await this.page.goto(uri, {
                    waitUntil: 'networkidle0'
                });

                const { cookies, uri: tranformedUri }: any = await this.registerCDPListener();
                const siteCookies = cookies.filter((cookie: any) => this.uri.includes(cookie.domain.slice(1)));
                console.log("executeHtml cookies" , siteCookies);
                this.requestOptions = { ...this.requestOptions , ...{
                    headers: {
                        Cookie: siteCookies.reduce((acc: any, {name, value}: any) => (`${acc}; ${name}=${value}`), '').slice(2)
                    }
                }}

                // this.requestOptions.method = "POST"

                // this.task.uri = tranformedUri;
                // this.uri = tranformedUri;

                resolve({cookies});
            } catch(e){
                this.emit(SPIDER_STATUS.SCRAPE_ERR, {
                    task: this.task, ...{
                        error: e
                    },
                    status: "PUPPETEER_FAIL"
                });
                reject({
                    ...{
                        error: e
                    },
                    status: "PUPPETEER_FAIL"
                });
            }
        })
    }

    /**
     * Scrape a give uri and give back the cheerio data
     * @returns {Promise<Cheerio>}
     */
    scrapeUri(): Promise<any> {
        const uri = this.uri;
        const userAgent = this.userAgent;
        return new Promise((resolve, reject) => {
            //console.log({ userAgent: userAgent.toString() })
            const options = {...{
                method: 'GET',
                url: uri,
                headers: {
                    'User-Agent': userAgent.toString()
                }
            }, ...this.requestOptions };
            this.request(options)
                .then( async ({ data }: any) => {
                    const $ = cheerio.load(data);
                    if(this.isCaptchaSite($)){
                        // await this.executeHtml();
                        console.log("to be implemented")
                    } else {
                        const body = this.tryJSONParse(data);
                        const result = {$, body};
                        this.emit(SPIDER_STATUS.SCRAPE_SUCCESS, {task: this.task, ...result});
                        resolve({ task: this.task, ...result, status: SPIDER_STATUS.SCRAPE_SUCCESS });
                    }
                })
                .catch(async (err: any) => {
                    const error = err.toString()
                    const body = err && err.response && err.response.data
                    const httpStatus = err && err.response && err.response.status
                    const requireCaptcha = body && (typeof body === "string") && body.includes('captcha');

                    if(requireCaptcha) { // if catpcha try to resolve the captcha
                        try {
                            await this.executeHtml(); //get the cookie back needed for query
                            // prepare form data
                            // const data = new FormData();
                            const $ = cheerio.load(body);
                            const formData = {
                                action: $('form.challenge-form').attr('action'),
                                // url: '/showthread.php?tid=9534',
                                r: $('input[name=r]').attr('value'),
                                cf_captcha_kind: $('input[name=cf_captcha_kind]').attr('value')
                            }
                            console.log(formData)
                            this.requestOptions = {
                                ...this.requestOptions, ...{
                                    data: qs.stringify(formData),
                                    method: $('form.challenge-form').attr('method'),
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    }
                                }
                            }
                            // console.log(this.requestOptions)
                            await this.scrapeUri(); // call self
                        }catch(e){
                            this.emit(SPIDER_STATUS.SCRAPE_ERR, {
                                task: this.task, ...{
                                    error,
                                    body,
                                    httpStatus,
                                    requireCaptcha
                                }
                            });
                            reject({
                                task: this.task, ...{
                                    e,
                                    body,
                                    httpStatus,
                                    requireCaptcha
                                }, status: SPIDER_STATUS.SCRAPE_ERR
                            });
                        }
                    }

                    this.emit(SPIDER_STATUS.SCRAPE_ERR, {
                        task: this.task, ...{
                            error,
                            body,
                            httpStatus,
                            requireCaptcha
                        }
                    });
                    reject({
                        task: this.task, ...{
                            error,
                            body,
                            httpStatus,
                            requireCaptcha
                        }, status: SPIDER_STATUS.SCRAPE_ERR
                    });
                });
        })
    };

    /**
     * download any resource to a given location
     * @param resourcePath
     */
    download = async ({ resourcePath }: { resourcePath : string }) => {
        return new Promise(async(resolve, reject) => {
            try {
                resourcePath = pathResolve(__dirname, resourcePath)
                if (!resourcePath) {
                    return reject({
                        task: this.task,
                        message: `resourcePath cannot be empty provide a valid value`,
                        status: SPIDER_STATUS.DOWNLOAD_ERR
                    });
                }

                await this.createDirIfNotExits({resourcePath})
                const progressBar = new cliProgress.Bar({}, cliProgress.Presets.shades_classic);
                const uri = this.uri;
                const request = this.request;
                const resourceFile = `${resourcePath}/${this.encodeUriToString(uri)}`;
                const userAgent = new UserAgent();
                const options = {
                    url: uri,
                    method: 'GET',
                    responseType: 'stream',
                    headers: {
                        'User-Agent': userAgent.toString()
                    }
                };
                const writer = fs.createWriteStream(resourcePath)
                await request({...options, method: 'HEAD'})
                const {data, headers} = await request(options);
                const totalLength = headers['content-length'];
                progressBar.start(totalLength, 0);

                data.pipe(writer);
                data.on('data', (chunk: any) => {
                    progressBar.update(chunk.length)
                })
                writer.on('finish', () => {
                    progressBar.stop();
                    // rename the file based on detected source mime
                    const resourceFileWithExt = `${resourcePath}/${this.task.name}.${this.mime}`;
                    fs.rename(resourceFile, resourceFileWithExt, (err) => {
                        if (err) {
                            this.emit(SPIDER_STATUS.DOWNLOAD_ERR, {
                                task: this.task,
                                error: err,
                                message: `error in renaming file from ${resourceFile} to ${resourceFileWithExt}`
                            });
                            reject({
                                task: this.task,
                                message: `error in renaming file from ${resourceFile} to ${resourceFileWithExt}`,
                                status: SPIDER_STATUS.DOWNLOAD_ERR
                            });
                        }
                        this.emit(SPIDER_STATUS.DOWNLOAD_SUCCESS, {
                            task: this.task,
                            message: `file downloaded ${resourceFileWithExt}`
                        });
                        resolve({
                            task: this.task,
                            message: `file downloaded ${resourceFileWithExt}`,
                            status: SPIDER_STATUS.DOWNLOAD_SUCCESS
                        })
                    });
                })
                writer.on('error', (requestErr: any) => {
                    this.emit(SPIDER_STATUS.DOWNLOAD_ERR, {
                        task: this.task,
                        error: requestErr,
                        message: `error in downloading file to ${resourcePath}`
                    });
                    reject({
                        task: this.task,
                        error: requestErr,
                        message: `error in downloading file to ${resourcePath}`,
                        status: SPIDER_STATUS.DOWNLOAD_ERR
                    });
                })
            }catch (e) {
                this.emit(SPIDER_STATUS.DOWNLOAD_ERR, {
                    task: this.task,
                    error: e,
                    message: `error in downloading file to ${resourcePath}`
                });
                reject({
                    task: this.task,
                    error: e,
                    message: `error in downloading file to ${resourcePath}`,
                    status: SPIDER_STATUS.DOWNLOAD_ERR
                });
            }
        })
    }
}