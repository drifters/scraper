import { curlBuilder } from '@drifters/curl-builder';

export const InitializeInterceptors = (axios: any) => {
    const axiosInstance = { ...axios };

    const requestInterceptor = (request: any) => {
        const config = { ...request };
        console.log('Request To Service:- ', curlBuilder(config, { inline: true }));
        return config;
    };

    const responseInterceptor = (response: any) => {

        return response;
    };

    const responseErrorInterceptor = async (error: any) => {
        let responseRef;
        return Promise.reject(error);
    };

    axiosInstance
        .interceptors
        .request
        .use(requestInterceptor, /* istanbul ignore next */ (error: any) => Promise.reject(error));
    axiosInstance
        .interceptors
        .response
        .use(responseInterceptor, /* istanbul ignore next */ responseErrorInterceptor);
    return { requestInterceptor, responseInterceptor, responseErrorInterceptor };
};
