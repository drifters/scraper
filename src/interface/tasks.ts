import {SPIDER_STATUS} from "..";

export enum ISpiderTaskType {
    EXECUTE_HTML = 'executeHtml',
    SCRAPE_URI = 'scrapeUri',
    DOWNLOAD = 'download'
}

export interface ISpiderTask {
    id ?: string | number,
    name: string,
    uri: string,
    type?: ISpiderTaskType;
    enableProxy?: boolean
    resourcePath?: string
}

export interface ISpiderError {
    status: SPIDER_STATUS;
    message: string;
    code ?: any;
    requireStack ?: any;
}