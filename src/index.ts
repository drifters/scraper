const  cleanup = require('./lib/cleanup').Cleanup(myCleanup);
//const config = require('@drifters/exit')
export { Spider } from './lib/spider';
export { ScraperJob } from './lib/job';
export { IScraper } from './interface/scraper';
export { ISpiderTask, ISpiderTaskType } from './interface/tasks';
export { SPIDER_JOB_STATUS, SPIDER_STATUS } from './lib/state';

function myCleanup() {
    console.log('App specific cleanup code...');
}

