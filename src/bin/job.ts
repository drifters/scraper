#!/usr/bin/env node
import {ScraperJob, SPIDER_JOB_STATUS} from '..';

try {
    const job = new ScraperJob({
        scraperName: "Amazing",
        enableProxy: true
    });

    job.on(SPIDER_JOB_STATUS.INITIALIZED,(data) => console.log(SPIDER_JOB_STATUS.INITIALIZED,data));
    job.on(SPIDER_JOB_STATUS.COMPLETED,(data) => console.log(SPIDER_JOB_STATUS.COMPLETED,data));
    job.on(SPIDER_JOB_STATUS.TASK_STARTED,(data) => console.log(SPIDER_JOB_STATUS.TASK_STARTED,data));
    job.on(SPIDER_JOB_STATUS.TASK_FINISHED,(data) => console.log(SPIDER_JOB_STATUS.TASK_FINISHED,data));
    job.on(SPIDER_JOB_STATUS.TASK_ERR,(data) => console.log(SPIDER_JOB_STATUS.TASK_ERR,data));

    job.scrape([{
        id: 1,
        name: "some weird name1",
        uri: "http://someUri3"
    },{
        id: 2,
        name: "some weird name2",
        uri: "https://api.myip.com/"
    }]);

}catch (e) {
    console.log(e);
}