# @drifters/scraper

> Scraper util for drifters project

[![Generic badge](https://img.shields.io/badge/bitbucket-@drifters/scraper-blue.svg)](https://bitbucket.org/bdrifter/drifters-library/src/master/packages/utils/scraper/)
[![TypeScript](https://img.shields.io/badge/</>-Typescript-blue.svg)](https://bitbucket.org/bdrifter/drifters-library/src/master/)
[![Build Status](https://travis-ci.com/drifters/scraper.svg?branch=master)](https://travis-ci.com/drifters/scraper)
[![codecov](https://codecov.io/bb/drifters/scraper/branch/master/graph/badge.svg?token=40B9GMJLGQ)](https://codecov.io/bb/drifters/scraper)
## Install

```bash
npm i --save @drifters/scraper
```

## Example


### Using a Spider

> Spider is to be used for scraping an instance of a resource
client has choice to download to get content and manipulate data over it

#### Example
> To scrape a URI
```javascript
import { Spider, SPIDER_STATUS } from '@drifters/scraper';

const spider = new Spider({
  id: "1",
  name: "SomePageName",
  uri: 'http://somesiteUrl',
  enableProxy: true // uses socks proxy if enabled NOTE: must run proxy separately
})

spider.scrapeUri();

spider.on(SPIDER_STATUS.SCRAPE_SUCCESS, ( { $ } ) => {
  //sample code depicting using cheerio to parse nodes to array
  const content = $('.entry-title > a')
    .filter(function () {
      return $(this).attr('href') !== '#';
    })
    .map(function () {
      return {
        uri: $(this).attr('href'),
        name: $(this).text(),
      };
    }).toArray();
  const nextLink = $('.nextpostslink').attr('href');
  console.log({ content, nextLink })
})

spider.on(SPIDER_STATUS.SCRAPE_ERR, ( err ) => {
  console.log(err)
})
```
> To download a URI
```javascript
import { Spider, SPIDER_STATUS } from '@drifters/scraper';

const spider = new Spider({
  id: "1",
  name: "SomePageName",
  uri: 'http://somesiteUrl',
})

//to download the uri to folder
spider.download({ resourcePath: 'download' });

spider.on(SPIDER_STATUS.DOWNLOAD_SUCCESS, ( data ) => {
  console.log(SPIDER_STATUS.DOWNLOAD_SUCCESS, data)
})

spider.on(SPIDER_STATUS.DOWNLOAD_ERR, ( err ) => {
  console.log(SPIDER_STATUS.DOWNLOAD_ERR, err)
});
```

 ## Event listeners available
> For a scraper job instance one can listen to following events

| Events | Usage 
| :--- | :--- | 
| SCRAPE_SUCCESS | spider successfully scrapes a uri
| SCRAPE_ERR | spider fails to scrape
| HEAD_ERR | fails to get the uri head requests
| DOWNLOAD_SUCCESS | on successful download of uri
| DOWNLOAD_ERR | on error for donwload


### Using a ScraperJob
> ScraperJob is to be used when you want to queue the scraping activities.
> Useful when scraping a site or downloading a bunch of resources

#### Example

```javascript
const { ScraperJob, SPIDER_JOB_STATUS } = require('@drifters/scraper');

const job = new ScraperJob({
    scraperName: "AmazingScraper"
});

job.on(SPIDER_JOB_STATUS.COMPLETED, () => {
    console.log("done");
});

job.on(SPIDER_JOB_STATUS.TASK_FINISHED, ({
    scraperName, // Job name choose by you 
    task, // same task that you gave
    data: {
        $, // a cheerio instance
        body // the response from the server
    }
}) => {
    console.log("called when a task finish");
    //@todo write you application logic of what you want with data
});

job.on(SPIDER_JOB_STATUS.TASK_ERR, ({
    scraperName, // Job name choose by you 
    task, // task object that you provided
    err // why the spider task failed
}) => {
    console.log("done", err);
    // queue the task back by using job.scrape([task])
    // or log to internal systems
});

job.scrape([{
    id: "1",
    name: "jestSite",
    uri: "https://jestjs.io/docs/en/configuration#transformignorepatterns-arraystring",
},{
    id: "2",
    name: "babelSite",
    uri: "https://babeljs.io/docs/en/babel-plugin-proposal-class-properties",
}]);

```
 ## Event listeners available
> For a scraper job instance one can listen to following events

| Events | Usage 
| :--- | :--- | 
| INITIALIZED |  job initialized
| COMPLETED | job completed 
| TASK_STARTED | task started
| TASK_ERR | task error
| TASK_FINISHED | task finished


## License

MIT © [suminksudhi](https://github.com/suminksudhi)
