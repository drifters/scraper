/// <reference types="../../types/ambient" />
import * as sinon from 'sinon';
import {assert} from 'chai'
import 'mocha';
import {ScraperJob, Spider, SPIDER_JOB_STATUS} from '../../src';

//https://marcofranssen.nl/using-mocha-chai-sinon-to-test-node-js/
//https://spin.atomicobject.com/2018/06/13/mock-typescript-modules-sinon/
//https://stackoverflow.com/questions/37343232/sinon-js-only-stub-a-method-once
describe('Job function', () => {
    let scraperJob: ScraperJob,
        sandbox: sinon.SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
       })

    afterEach(() => {
        sandbox.restore();
    })

    before(() => {
        scraperJob = new ScraperJob({
            scraperName: "SomeJobName",
            enableProxy: true
        });
    })

    it('must run the jobs provided to api',  async () => {
        sinon.stub(Spider.prototype, "scrapeUri").resolves({body:{"a":1}});
        const listenerSpy = sinon.spy(scraperJob, "on")
        const listenerHandlerSpy = sinon.spy()

        scraperJob.scrape([{
            id: 1,
            name: "some weird name1",
            uri: "http://someUri1"
        },{
            id: 2,
            name: "some weird name1",
            uri: "http://someUri2"
        }])


        scraperJob.on(SPIDER_JOB_STATUS.TASK_STARTED, listenerHandlerSpy)
        scraperJob.on(SPIDER_JOB_STATUS.TASK_FINISHED, listenerHandlerSpy);
        scraperJob.on(SPIDER_JOB_STATUS.COMPLETED, listenerHandlerSpy);

        assert.equal(listenerSpy.calledWithExactly(SPIDER_JOB_STATUS.COMPLETED, listenerHandlerSpy),true)
        assert.equal(listenerSpy.calledWithExactly(SPIDER_JOB_STATUS.TASK_STARTED, listenerHandlerSpy),true)
        assert.equal(listenerSpy.calledWithExactly(SPIDER_JOB_STATUS.TASK_FINISHED,listenerHandlerSpy),true)
    });
});

process.on('unhandledRejection', (e: any) => {
    console.log('you forgot to return a Promise! Check your tests!' + e.message)
})