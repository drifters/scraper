const { ScraperJob, SPIDER_JOB_STATUS } = require('../dist');

const job = new ScraperJob({
    scraperName: "AmazingScraper"
});
job.on(SPIDER_JOB_STATUS.COMPLETED, () => {
    console.log("done");
})

job.on(SPIDER_JOB_STATUS.TASK_FINISHED, (args) => {
    console.log("done", args);
})

job.on(SPIDER_JOB_STATUS.TASK_ERR, (err) => {
    console.log("done", err);
})

job.scrape([{
    id: "1",
    name: "jestSite",
    uri: "https://jestjs.io/docs/en/configuration#transformignorepatterns-arraystring",
},{
    id: "2",
    name: "babelSite",
    uri: "https://babeljs.io/docs/en/babel-plugin-proposal-class-properties",
}])

